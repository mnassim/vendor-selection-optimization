package com.company.optimization;

import com.company.DataImporter;
import com.company.Purchasing;
import com.company.ResultExporter;
import gurobi.*;

import java.io.IOException;
import java.util.ArrayList;

public class LexicographicOptimization {

    ArrayList<Purchasing> purchasingsData;
    DataImporter dataImporter;
    ResultExporter resultExporter;

    public LexicographicOptimization() {
        this.dataImporter = new DataImporter();
        this.purchasingsData = this.dataImporter.importDataFromFile();
        this.resultExporter = new ResultExporter();
    }

    public void optimize() {
        try {

            // order number
            int n = 7381;

            double[] c1 = purchasingsData.stream().mapToDouble(p -> p.getSupplierOneCost()).toArray();
            double[] c2 = purchasingsData.stream().mapToDouble(p -> p.getSupplierTwoCost()).toArray();
            double[] c3 = purchasingsData.stream().mapToDouble(p -> p.getSupplierThreeCost()).toArray();

            double[] q1 = purchasingsData.stream().mapToDouble(p -> p.getSupplierOneQual()).toArray();
            double[] q2 = purchasingsData.stream().mapToDouble(p -> p.getSupplierTwoQual()).toArray();
            double[] q3 = purchasingsData.stream().mapToDouble(p -> p.getSupplierThreeQual()).toArray();

            double[] d1 = purchasingsData.stream().mapToDouble(p -> p.getSupplierOneOtd()).toArray();
            double[] d2 = purchasingsData.stream().mapToDouble(p -> p.getSupplierTwoOtd()).toArray();
            double[] d3 = purchasingsData.stream().mapToDouble(p -> p.getSupplierThreeOtd()).toArray();

            GRBEnv env = new GRBEnv(true);
            env.set("logFile", "lexicographic.log");
            env.start();

            GRBModel model = new GRBModel(env);
            GRBVar[] x = model.addVars(n, GRB.BINARY);
            GRBVar[] y = model.addVars(n, GRB.BINARY);
            GRBVar[] z = model.addVars(n, GRB.BINARY);

            model.set(GRB.IntAttr.ModelSense, GRB.MINIMIZE);
            GRBLinExpr objective1 = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                objective1.addTerm(c1[i],x[i]);
                objective1.addTerm(c2[i],y[i]);
                objective1.addTerm(c3[i],z[i]);
            }

            GRBLinExpr objective2 = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                objective2.addTerm(q1[i],x[i]);
                objective2.addTerm(q2[i],y[i]);
                objective2.addTerm(q3[i],z[i]);
            }
            GRBLinExpr objective3 = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                objective3.addTerm(d1[i],x[i]);
                objective3.addTerm(d2[i],y[i]);
                objective3.addTerm(d3[i],z[i]);
            }

            model.setObjectiveN(objective1,1,2,1,0,0,"priceObjective");
            model.setObjectiveN(objective2,2,3,-1,0,0,"qualObjective");
            model.setObjectiveN(objective3,3,1,-1,0,0,"deliveryObjective");

            // all the orders must be assigned
            GRBLinExpr constraint0 = new GRBLinExpr();
            for (int j = 0; j<n ;j++){
                constraint0.addTerm(1.0,x[j]);
                constraint0.addTerm(1.0,y[j]);
                constraint0.addTerm(1.0,z[j]);
            }
            model.addConstr(constraint0,GRB.EQUAL, n, "constraint0");

            GRBLinExpr constraint1 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint1.clear();
                constraint1.addTerm(1.0,x[j]);
                constraint1.addTerm(1.0,y[j]);
                constraint1.addTerm(1.0,z[j]);
                model.addConstr(constraint1,GRB.EQUAL, 1, "c1"+j);

            }

            GRBLinExpr constraint2 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint2.addTerm(1.0,x[j]);
            }
            model.addConstr(constraint2,GRB.GREATER_EQUAL, 0.15 * n, "c2");

            GRBLinExpr constraint3 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint3.addTerm(1.0,y[j]);
            }
            model.addConstr(constraint3,GRB.GREATER_EQUAL, 0.15 * n, "c2");

            GRBLinExpr constraint4 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint4.addTerm(1.0,z[j]);
            }
            model.addConstr(constraint4,GRB.GREATER_EQUAL, 0.15 * n, "c2");


            model.write("Multiobj-lexicographic.lp");

            model.optimize();

            ArrayList<String[]> optimizationResult = new ArrayList<>();
            optimizationResult.add(new String[]
                    {"id", "affected to supp 1","affected to supp 2","affected to supp 3" });
            double priceResult = 0;
            double qualityScore = 0;
            double deliveryScore = 0;
            int ordersAssignedToSupplier1 = 0;
            int ordersAssignedToSupplier2 = 0;
            int ordersAssignedToSupplier3 = 0;
            for (int j = 0; j < n; j++) {
                ordersAssignedToSupplier1 += x[j].get(GRB.DoubleAttr.X);
                ordersAssignedToSupplier2 += y[j].get(GRB.DoubleAttr.X);
                ordersAssignedToSupplier3 += z[j].get(GRB.DoubleAttr.X);

                priceResult += x[j].get(GRB.DoubleAttr.X) * c1[j] + y[j].get(GRB.DoubleAttr.X) * c2[j] + z[j].get(GRB.DoubleAttr.X) * c3[j];
                qualityScore += x[j].get(GRB.DoubleAttr.X) * q1[j] + y[j].get(GRB.DoubleAttr.X) * q2[j] + z[j].get(GRB.DoubleAttr.X) * q3[j];
                deliveryScore += x[j].get(GRB.DoubleAttr.X) * d1[j] + y[j].get(GRB.DoubleAttr.X) * d2[j] + z[j].get(GRB.DoubleAttr.X) * d3[j];
                optimizationResult.add(new String[]
                        {String.valueOf(j), String.valueOf(x[j].get(GRB.DoubleAttr.X)), String.valueOf(y[j].get(GRB.DoubleAttr.X)), String.valueOf(z[j].get(GRB.DoubleAttr.X)) });
            }


            System.out.println("total price :" + priceResult/n);
            System.out.println("quality Score :" + qualityScore/n);
            System.out.println("delivery Score :" + deliveryScore/n);
            System.out.println("ordersAssignedToSupplier1 :" + Double.valueOf(ordersAssignedToSupplier1)/n);
            System.out.println("ordersAssignedToSupplier2 :" + Double.valueOf(ordersAssignedToSupplier2)/n);
            System.out.println("ordersAssignedToSupplier3 :" + Double.valueOf(ordersAssignedToSupplier3)/n);
            this.resultExporter.DataToCSV("lexicographicResults",optimizationResult);
            System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));

            // Dispose of model and environment
            model.dispose();
            env.dispose();

        }
        catch (GRBException e){
            System.err.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
