package com.company.optimization;

import com.company.DataImporter;
import com.company.Purchasing;
import com.company.ResultExporter;
import gurobi.*;

import java.io.IOException;
import java.util.ArrayList;

public class GoalPrograming {
    ArrayList<Purchasing> purchasingsData;
    DataImporter dataImporter;
    ResultExporter resultExporter;

    public GoalPrograming() {
        this.dataImporter = new DataImporter();
        this.purchasingsData = this.dataImporter.importDataFromFile();
        this.resultExporter = new ResultExporter();
    }

    public void optimize() {
        try {

            // order number
            int n = 7381;

            double[] c1 = purchasingsData.stream().mapToDouble(p -> p.getSupplierOneCost()).toArray();
            double[] c2 = purchasingsData.stream().mapToDouble(p -> p.getSupplierTwoCost()).toArray();
            double[] c3 = purchasingsData.stream().mapToDouble(p -> p.getSupplierThreeCost()).toArray();

            double[] q1 = purchasingsData.stream().mapToDouble(p -> p.getSupplierOneQual()).toArray();
            double[] q2 = purchasingsData.stream().mapToDouble(p -> p.getSupplierTwoQual()).toArray();
            double[] q3 = purchasingsData.stream().mapToDouble(p -> p.getSupplierThreeQual()).toArray();

            double[] d1 = purchasingsData.stream().mapToDouble(p -> p.getSupplierOneOtd()).toArray();
            double[] d2 = purchasingsData.stream().mapToDouble(p -> p.getSupplierTwoOtd()).toArray();
            double[] d3 = purchasingsData.stream().mapToDouble(p -> p.getSupplierThreeOtd()).toArray();

           double priceGoalValue = purchasingsData.stream().mapToDouble(p-> p.getExpectedPrice()).sum();
            System.out.println("total price goal = " + priceGoalValue/n);
            GRBEnv env = new GRBEnv(true);
            env.set("logFile", "goalProgramming.log");
            env.start();

            GRBModel model = new GRBModel(env);
            GRBVar[] x = model.addVars(n, GRB.BINARY);
            GRBVar[] y = model.addVars(n, GRB.BINARY);
            GRBVar[] z = model.addVars(n, GRB.BINARY);

            GRBVar[] pricePositiveDevation = model.addVars(n, GRB.BINARY);
            GRBVar[] qualityPositiveDevation = model.addVars(n, GRB.BINARY);
            GRBVar[] deliveryPositiveDevation = model.addVars(n, GRB.BINARY);

            GRBVar[] priceNegativeDevation = model.addVars(n, GRB.SEMICONT);
            GRBVar[] qualityNegativeDevation = model.addVars(n, GRB.CONTINUOUS);
            GRBVar[] deliveryNegativeDevation = model.addVars(n, GRB.CONTINUOUS);


            GRBLinExpr objective = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                objective.addTerm(1,pricePositiveDevation[i]);
                objective.addTerm(1,qualityNegativeDevation[i]);
                objective.addTerm(1,deliveryNegativeDevation[i]);
            }

            model.setObjective(objective,GRB.MINIMIZE);


            // all the orders must be assigned
            GRBLinExpr constraint0 = new GRBLinExpr();
            for (int j = 0; j<n ;j++){
                constraint0.addTerm(1.0,x[j]);
                constraint0.addTerm(1.0,y[j]);
                constraint0.addTerm(1.0,z[j]);
            }
            model.addConstr(constraint0,GRB.EQUAL, n, "constraint0");

            // each order can only be assigned to one supplier
            GRBLinExpr constraint1 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint1.clear();
                constraint1.addTerm(1.0,x[j]);
                constraint1.addTerm(1.0,y[j]);
                constraint1.addTerm(1.0,z[j]);
                model.addConstr(constraint1,GRB.EQUAL, 1, "c1"+j);

            }

            // min proportion for first supplier
            GRBLinExpr constraint2 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint2.addTerm(1.0,x[j]);
            }
            model.addConstr(constraint2,GRB.GREATER_EQUAL, 0.15 * n, "c2");
            // min proportion for second supplier
            GRBLinExpr constraint3 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint3.addTerm(1.0,y[j]);
            }
            model.addConstr(constraint3,GRB.GREATER_EQUAL, 0.15 * n, "c2");

            // min proportion for third supplier
            GRBLinExpr constraint4 = new GRBLinExpr() ;
            for (int j = 0; j<n ;j++){
                constraint4.addTerm(1.0,z[j]);
            }
            model.addConstr(constraint4,GRB.GREATER_EQUAL, 0.15 * n, "c2");

            //goal programming constraints
            GRBLinExpr PriceGoal = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                PriceGoal.addTerm(c1[i],x[i]);
                PriceGoal.addTerm(c2[i],y[i]);
                PriceGoal.addTerm(c3[i],z[i]);
                PriceGoal.addTerm(-1, pricePositiveDevation[i]);
                PriceGoal.addTerm(1, priceNegativeDevation[i]);
            }
            model.addConstr(PriceGoal,GRB.EQUAL, priceGoalValue, "PriceGoal");


            GRBLinExpr QualityGoal = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                QualityGoal.addTerm(q1[i],x[i]);
                QualityGoal.addTerm(q2[i],y[i]);
                QualityGoal.addTerm(q3[i],z[i]);
                QualityGoal.addTerm(-1, qualityPositiveDevation[i]);
                QualityGoal.addTerm(1, qualityNegativeDevation[i]);
            }
            model.addConstr(QualityGoal,GRB.EQUAL, 0.85 * n, "QualityGoal");

            GRBLinExpr DeliveryGoal = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                DeliveryGoal.addTerm(d1[i],x[i]);
                DeliveryGoal.addTerm(d2[i],y[i]);
                DeliveryGoal.addTerm(d3[i],z[i]);
                DeliveryGoal.addTerm(d3[i],z[i]);
                DeliveryGoal.addTerm(-1, deliveryPositiveDevation[i]);
                DeliveryGoal.addTerm(1, deliveryNegativeDevation[i]);
            }
            model.addConstr(DeliveryGoal,GRB.EQUAL, 0.85 * n, "DeliveryGoal");

            //deviation constraints
            GRBLinExpr PositivePriceDeviationsConstraint = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                PositivePriceDeviationsConstraint.clear();
                PositivePriceDeviationsConstraint.addTerm(1,pricePositiveDevation[i]);
                model.addConstr(PositivePriceDeviationsConstraint,GRB.GREATER_EQUAL, 0, "PositivePricedeviationConstraint" + i);

            }
            GRBLinExpr NegativePriceDeviationsConstraint = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                NegativePriceDeviationsConstraint.clear();
                NegativePriceDeviationsConstraint.addTerm(1,priceNegativeDevation[i]);
                model.addConstr(NegativePriceDeviationsConstraint,GRB.GREATER_EQUAL, 0, "NegativePricedeviationConstraint" + i);

            }
            GRBLinExpr NegativeDeliveryDeviationsConstraint = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                NegativeDeliveryDeviationsConstraint.clear();
                NegativeDeliveryDeviationsConstraint.addTerm(1,deliveryNegativeDevation[i]);
                model.addConstr(NegativeDeliveryDeviationsConstraint,GRB.GREATER_EQUAL, 0, "NegativeDeliveryDeviationsConstraint" + i);

            }
            GRBLinExpr PositiveDeliveryDeviationsConstraint = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                PositiveDeliveryDeviationsConstraint.clear();
                PositiveDeliveryDeviationsConstraint.addTerm(1,deliveryPositiveDevation[i]);
                model.addConstr(PositiveDeliveryDeviationsConstraint,GRB.GREATER_EQUAL, 0, "PositiveDeliveryDeviationsConstraint" + i);

            }
            GRBLinExpr NegativeQualityDeviationsConstraint = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                NegativeQualityDeviationsConstraint.clear();
                NegativeQualityDeviationsConstraint.addTerm(1,qualityNegativeDevation[i]);
                model.addConstr(NegativeQualityDeviationsConstraint,GRB.GREATER_EQUAL, 0, "NegativeQualityDeviationsConstraint" + i);

            }
            GRBLinExpr PositiveQualityDeviationsConstraint = new GRBLinExpr();
            for (int i = 0; i<n ;i++){
                PositiveQualityDeviationsConstraint.clear();
                PositiveQualityDeviationsConstraint.addTerm(1,qualityPositiveDevation[i]);
                model.addConstr(PositiveQualityDeviationsConstraint,GRB.GREATER_EQUAL, 0, "PositiveQualityDeviationsConstraint" + i);

            }


            model.write("Goal-programming.lp");

            model.optimize();

            ArrayList<String[]> optimizationResult = new ArrayList<>();
            optimizationResult.add(new String[]
                    {"id", "affected to supp 1","affected to supp 2","affected to supp 3" });
            double priceResult = 0;
            double qualityScore = 0;
            double deliveryScore = 0;
            int ordersAssignedToSupplier1 = 0;
            int ordersAssignedToSupplier2 = 0;
            int ordersAssignedToSupplier3 = 0;
            for (int j = 0; j < n; j++) {
                ordersAssignedToSupplier1 += x[j].get(GRB.DoubleAttr.X);
                ordersAssignedToSupplier2 += y[j].get(GRB.DoubleAttr.X);
                ordersAssignedToSupplier3 += z[j].get(GRB.DoubleAttr.X);
                priceResult += x[j].get(GRB.DoubleAttr.X) * c1[j] + y[j].get(GRB.DoubleAttr.X) * c2[j] + z[j].get(GRB.DoubleAttr.X) * c3[j];
                qualityScore += x[j].get(GRB.DoubleAttr.X) * q1[j] + y[j].get(GRB.DoubleAttr.X) * q2[j] + z[j].get(GRB.DoubleAttr.X) * q3[j];
                deliveryScore += x[j].get(GRB.DoubleAttr.X) * d1[j] + y[j].get(GRB.DoubleAttr.X) * d2[j] + z[j].get(GRB.DoubleAttr.X) * d3[j];
                optimizationResult.add(new String[]
                        {String.valueOf(j), String.valueOf(x[j].get(GRB.DoubleAttr.X)), String.valueOf(y[j].get(GRB.DoubleAttr.X)), String.valueOf(z[j].get(GRB.DoubleAttr.X)) });
            }


            System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal));
            System.out.println("total price :" + priceResult/n);
            System.out.println("quality Score :" + qualityScore/n);
            System.out.println("delivery Score :" + deliveryScore/n);
            System.out.println("ordersAssignedToSupplier1 :" + Double.valueOf(ordersAssignedToSupplier1)/n);
            System.out.println("ordersAssignedToSupplier2 :" + Double.valueOf(ordersAssignedToSupplier2)/n);
            System.out.println("ordersAssignedToSupplier3 :" + Double.valueOf(ordersAssignedToSupplier3)/n);
            this.resultExporter.DataToCSV("goalProgramingResults",optimizationResult);

            // Dispose of model and environment
            model.dispose();
            env.dispose();

        }
        catch (GRBException e){
            System.err.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
