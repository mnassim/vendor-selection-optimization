package com.company;

public class Purchasing {
    private int id;

    private String reference;

    private Double SupplierOneCost;
    private Double SupplierTwoCost;
    private Double SupplierThreeCost;

    private Double SupplierOneQual;
    private Double SupplierTwoQual;
    private Double SupplierThreeQual;

    private Double SupplierOneOtd;
    private Double SupplierTwoOtd;
    private Double SupplierThreeOtd;
    private Double ExpectedPrice;

    public Purchasing(int id, String reference, Double expectedPrice, Double supplierOneCost, Double supplierTwoCost, Double supplierThreeCost, Double supplierOneQual, Double supplierTwoQual, Double supplierThreeQual, Double supplierOneOtd, Double supplierTwoOtd,Double supplierThreeOtd) {
        this.id = id;
        this.ExpectedPrice =expectedPrice;
        this.reference = reference;
        SupplierOneCost = supplierOneCost;
        SupplierTwoCost = supplierTwoCost;
        SupplierThreeCost = supplierThreeCost;
        SupplierOneQual = supplierOneQual;
        SupplierTwoQual = supplierTwoQual;
        SupplierThreeQual = supplierThreeQual;
        SupplierOneOtd = supplierOneOtd;
        SupplierTwoOtd = supplierTwoOtd;
        SupplierThreeOtd = supplierThreeOtd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getSupplierOneCost() {
        return SupplierOneCost;
    }

    public void setSupplierOneCost(Double supplierOneCost) {
        SupplierOneCost = supplierOneCost;
    }

    public Double getSupplierTwoCost() {
        return SupplierTwoCost;
    }

    public void setSupplierTwoCost(Double supplierTwoCost) {
        SupplierTwoCost = supplierTwoCost;
    }

    public Double getSupplierThreeCost() {
        return SupplierThreeCost;
    }

    public void setSupplierThreeCost(Double supplierThreeCost) {
        SupplierThreeCost = supplierThreeCost;
    }

    public Double getSupplierOneQual() {
        return SupplierOneQual;
    }

    public void setSupplierOneQual(Double supplierOneQual) {
        SupplierOneQual = supplierOneQual;
    }

    public Double getSupplierTwoQual() {
        return SupplierTwoQual;
    }

    public void setSupplierTwoQual(Double supplierTwoQual) {
        SupplierTwoQual = supplierTwoQual;
    }

    public Double getSupplierThreeQual() {
        return SupplierThreeQual;
    }

    public void setSupplierThreeQual(Double supplierThreeQual) {
        SupplierThreeQual = supplierThreeQual;
    }

    public Double getSupplierOneOtd() {
        return SupplierOneOtd;
    }

    public void setSupplierOneOtd(Double supplierOneOtd) {
        SupplierOneOtd = supplierOneOtd;
    }

    public Double getSupplierTwoOtd() {
        return SupplierTwoOtd;
    }

    public void setSupplierTwoOtd(Double supplierTwoOtd) {
        SupplierTwoOtd = supplierTwoOtd;
    }

    public Double getSupplierThreeOtd() {
        return SupplierThreeOtd;
    }

    public void setSupplierThreeOtd(Double supplierThreeOtd) {
        SupplierThreeOtd = supplierThreeOtd;
    }

    public Double getExpectedPrice() {
        return ExpectedPrice;
    }

    public void setExpectedPrice(Double expectedPrice) {
        ExpectedPrice = expectedPrice;
    }
}
