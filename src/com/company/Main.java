package com.company;

import com.company.optimization.GoalPrograming;
import com.company.optimization.LexicographicOptimization;
import com.company.optimization.WeightedSumOptimization;

public class Main {

    public static void main(String[] args) {
//
        WeightedSumOptimization weightedSumOptimization = new WeightedSumOptimization();
        weightedSumOptimization.optimize();
//        LexicographicOptimization lexicographicOptimization = new LexicographicOptimization();
//        lexicographicOptimization.optimize();
//        GoalPrograming goalPrograming = new GoalPrograming();
//        goalPrograming.optimize();

    }
}
