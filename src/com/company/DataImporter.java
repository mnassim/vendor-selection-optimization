package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;


public class DataImporter {

    public ArrayList<Purchasing> importDataFromFile(){
            ArrayList<Purchasing> purchasingData = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\nassim.medjnoun\\Documents\\cours\\projets\\icape-vendor-selection\\src\\com\\company\\purchasing.csv"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                purchasingData.add(new Purchasing(Integer.valueOf(values[0]),values[1],Double.valueOf(values[3]),Double.valueOf(values[4]),Double.valueOf(values[5]),Double.valueOf(values[6]),Double.valueOf(values[7]),Double.valueOf(values[8]),Double.valueOf(values[9]),Double.valueOf(values[10]),Double.valueOf(values[11]),Double.valueOf(values[12])));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return purchasingData;
    }
}
